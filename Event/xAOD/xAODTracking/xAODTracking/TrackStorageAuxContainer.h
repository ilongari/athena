/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_TRACKSTORAGEAUXCONTAINER_H
#define XAODTRACKING_TRACKSTORAGEAUXCONTAINER_H

#include "xAODTracking/versions/TrackStorageAuxContainer_v1.h"

namespace xAOD {
  typedef TrackStorageAuxContainer_v1 TrackStorageAuxContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackStorageAuxContainer , 1219790473 , 1 )
#endif